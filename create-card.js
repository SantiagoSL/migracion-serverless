const AWS = require('aws-sdk');
const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });

exports.handler = async (event) => {
  console.log(event)
  const queue = event.Records.map((record) => JSON.parse(record.body))

  for (const item of queue) {
    console.log(item)
    const body = JSON.parse(JSON.parse(item.Message).body)
    const type = calculateAge(body.birthdate) > 45 ? 'gold' : 'classic';
    const dbParams = {
      ExpressionAttributeNames: {
        "#card": "creditCard",
      },
      ExpressionAttributeValues: {
        ":card": {
          M: {
            "number": {
              S: '1234-4567-8910',
            },
            "expiration": {
              S: '02/25',
            },
            "ccv": {
              S: '223',
            },
            "type":{
              S: type
            }
          },
        },
      },
      Key: {
        dni: {
          S: body.dni,
        },
      },
      TableName: process.env.CLIENTS_TABLE,
      UpdateExpression: "SET #card = :card"
    };
    console.log(dbParams)
    try {
      const dbResult = await dynamodb.updateItem(dbParams).promise();
      console.log(dbResult)
  
    } catch (e) {
      console.error(e);
    }
  }
  const response = {
      statusCode: 200,
      body: JSON.stringify('Successfully assigned gift'),
  };
  return response;
};

function calculateAge(date) { 
  const birthdate = new Date(date);
  const diff = Date.now() - birthdate.getTime();
  const ageDate = new Date(diff);
  return Math.abs(ageDate.getUTCFullYear() - 1970);
}