const AWS = require('aws-sdk');
const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });

exports.handler = async (event) => {
  console.log(event);
  const age = getAge(event.birthDate);
  if(age > 65 || age < 18) {
    const response = {
      statusCode: 400,
      body: 'Age must be between 18 and 65 years.'
    }
    return response;
  }
  const params = {
    Item: {
      dni: {
        S: event.dni
      },
      lastName: {
        S: event.lastName
      },
      name: {
        S: event.name
      },
      birthDate: {
        S: event.birthDate
      }
    },
    ReturnConsumedCapacity: "TOTAL",
    TableName: process.env.CLIENTS_TABLE
  };
  try {
    const dbResult = await dynamodb.putItem(params).promise();
    console.log(dbResult)

  } catch (e) {
    console.error(e);
  }
  
  const sns = new AWS.SNS();
  const topicParams = {
    Message: JSON.stringify(event),
    Subject: 'client created',
    TopicArn: process.env.SNS_TOPIC_ARN
  };
  const snsResult = await sns.publish(topicParams).promise();
  console.log("sns publish",snsResult)
  const response = {
    statusCode: 200,
    body: JSON.stringify(event),
  };
  return response;
};

function getAge(birthDay) {
    const birthDate = new Date(birthDay)
    var ageDifMs = Date.now() - birthDate.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}