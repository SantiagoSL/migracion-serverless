const AWS = require('aws-sdk');
const DYNAMODB = require("aws-sdk/clients/dynamodb");
const dynamodb = new DYNAMODB({ region: "us-east-1" });

exports.handler = async (event) => {
  console.log(event)
  const queue = event.Records.map((record) => JSON.parse(record.body))

  for (const item of queue) {
    const body = JSON.parse(JSON.parse(item.Message).body)
    let gift;
    if(isAutumn(body.birthDate)) {
      gift = 'buzo';
    }
    if(isWinter(body.birthDate)) {
      gift = 'sweater';
    }
    if(isSpring(body.birthDate)) {
      gift = 'camisa';
    }
    if(isSummer(body.birthDate)) {
      gift = 'remera';
    }
    const params = {
      TableName: process.env.CLIENTS_TABLE,
      ExpressionAttributeNames: {
        "#G": "gift",
      }, 
      UpdateExpression: 'set  #G = :G',
      ExpressionAttributeValues: {
        ':G':  {S:gift}
      },
      Key: {
        dni: {S: body.dni}
      },
      ReturnConsumedCapacity: "TOTAL"
    };
    console.log(params)
    try {
      const dbResult = await dynamodb.updateItem(params).promise();
      console.log(dbResult)
  
    } catch (e) {
      console.error(e);
    }
  }
  const response = {
      statusCode: 200,
      body: JSON.stringify('Successfully assigned gift'),
  };
  return response;
};

function isAutumn(birthDate) {
  const dt = new Date(birthDate);
  const m = dt.getMonth();
  return m === 2 ? dt.getUTCDate() >= 21 : m === 5 ? dt.getUTCDate() < 21 : m < 5 && m > 2;
}

function isWinter(birthDate) {
  const dt = new Date(birthDate);
  const m = dt.getMonth();
  return m === 5 ? dt.getUTCDate() >= 21 : m === 8 ? dt.getUTCDate() < 21 : m < 5 && m > 8;
}

function isSpring(birthDate) {
  const dt = new Date(birthDate);
  const m = dt.getMonth();
  return m === 8 ? dt.getUTCDate() >= 21 : m === 11 ? dt.getUTCDate() < 21 : m < 8 && m > 11;
}

function isSummer(birthDate) {
  const dt = new Date(birthDate);
  const m = dt.getMonth();
  return m === 11 ? dt.getUTCDate() >= 21 : m === 2 ? dt.getUTCDate() < 21 : m < 11 && m > 8;
}